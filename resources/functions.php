<?php

/**
 * Do not edit anything in this file unless you know what you're doing
 */

use Roots\Sage\Config;
use Roots\Sage\Container;

// Event organiser

add_filter( 'eventorganiser_admin_fullcalendar_event', 'vizit_callback_function', 10, 3 );
function vizit_callback_function( $event, $event_id, $occurrence_id ) {
    $venues = implode(', ', get_post_meta($event_id, '_venues', true));
    if(empty($venues)) {
        $venues = eo_get_venue_name($event['venue']);
    }
    $loc = "\n\nLocatie(s): ". $venues;
    $event['title'] = $event['title'] . $loc;
    $event['venues'] = get_post_meta($event_id, 'locations', true) ?: array(strval($event['venue']));
	return $event;
}

// Custom rules for posting

function force_type_private($post) {
    if ($post['post_type'] === 'event' && !current_user_can('edit_others_events')) { 
        if ($post['post_status'] != 'trash') {
            $post['post_status'] = 'private';
        }
    }
    return $post;
}

add_filter('wp_insert_post_data', 'force_type_private');

/* Pass body classes */

add_filter( 'body_class', 'vizit_body_class' );
function vizit_body_class( $classes ) {
    if( is_page('contact') || (is_single() && get_post_type() === 'aanbod') ) {
        $classes[] = 'has-map';
    }
    return $classes;
}

/* Add css to admin */

add_action('admin_head', 'vizit_custom_eo');

function vizit_custom_eo() {
    echo '<style> .eo-venue-combobox-select {display: none;}</style>';
}

/* Add time support for WPCF7 */

add_action( 'wpcf7_init', 'custom_add_form_tag_time' );
 
function custom_add_form_tag_time() {
    wpcf7_add_form_tag(
        array( 'time', 'time*'),
        'wpcf7_text_form_tag_handler',
        array( 'name-attr' => true )
    );
}

function vizit_cf7_select_values($tag) {
    if ($tag['basetype'] != 'select') {
        return $tag;
    }

    $values = [];
    $labels = [];
    foreach ($tag['raw_values'] as $raw_value) {
        $raw_value_parts = explode('|', $raw_value);
        if (count($raw_value_parts) >= 2) {
            $values[] = $raw_value_parts[1];
            $labels[] = $raw_value_parts[0];
        } else {
            $values[] = $raw_value;
            $labels[] = $raw_value;
        }
    }
    $tag['values'] = $values;
    $tag['labels'] = $labels;

    // Optional but recommended:
    //    Display labels in mails instead of values
    //    You can still use values using [_raw_tag] instead of [tag]
    $reversed_raw_values = array_map(function ($raw_value) {
        $raw_value_parts = explode('|', $raw_value);
        return implode('|', array_reverse($raw_value_parts));
    }, $tag['raw_values']);
    $tag['pipes'] = new \WPCF7_Pipes($reversed_raw_values);

    return $tag;
}
add_filter('wpcf7_form_tag', 'vizit_cf7_select_values', 10);

/* Add Gozer link */
add_action( 'admin_menu', function() {
    add_menu_page( 'linked_url', 'De Gozer', 'read', 'gozer', '', 'dashicons-money-alt', 1 );
});

add_action( 'admin_menu' , function() {
    global $menu;
    $menu[1][2] = "https://www.degozer.be";
});

/* Add privacylink shortcode */

add_shortcode('privacy_link', function() {
    return get_the_privacy_policy_link();
});

/* Setup option fields */

if( function_exists('acf_add_options_page') ) {
    acf_add_options_page('Vizit instellingen');
    acf_add_options_page('Thema instellingen');
}

/* Shortcodes */

function shortcode_button( $atts, $content = null ) {
    $a = shortcode_atts( array(
        'href' => site_url(),
        'type' => 'primary',
        'target' => '_self',
        'outline' => false,
    ), $atts );

    return '<a href="'. $a['href'] .'" target="'. $a['target'] .'" class="btn btn-'. $a['type'] .'">'. $content .'</a>';
}
add_shortcode( 'button', 'shortcode_button' );

add_filter( 'wpcf7_form_elements', 'vizit_wpcf7_form_elements' );

function vizit_wpcf7_form_elements( $form ) {
    $form = do_shortcode( $form );
    return $form;
}

/* ACF Pass data */

add_filter('sage/blocks/news-headlines/data', function ($block) {
    $query = array(
        'post_type' => 'post',
        'orderby' => 'date',
        'order' => 'DESC',
        'posts_per_page' => get_field('count') ?? 3,
    );

    $block['news'] = new WP_Query($query);
    return $block;
});

add_filter('sage/blocks/latest-events/data', function ($block) {
    $args = array(
        'event_start_after' => 'today',
        'numberposts'       => get_field('count') ?? 5,
      );

    if(get_field('filter_category')) {
        $args['tax_query'][] = array(
                'taxonomy'  => 'event-category',
                'operator'  => 'IN',
                'field'     => 'id',
                'terms'     => get_field('filter_category'),
        );
    }

    if(get_field('filter_tags')) {
        $args['tax_query'][] = array(
                'taxonomy'  => 'event-tag',
                'operator'  => 'IN',
                'field'     => 'id',
                'terms'     => get_field('filter_tags'),
        );
    }

    if(get_field('filter_tags_not')) {
        $args['tax_query'][] = array(
                'taxonomy'  => 'event-tag',
                'operator'  => 'NOT IN',
                'field'     => 'id',
                'terms'     => get_field('filter_tags_not'),
        );
    }

    $block['events'] = eo_get_events($args);
    return $block;
});

add_filter('sage/blocks/overview-pages/data', function ($block) {
    $query = array(
        'post_type' => get_field('type_page'),
        'orderby' => 'menu_order',
        'order' => 'DESC',
        'posts_per_page' => -1,
    );

    $block['page'] = new WP_Query($query);
    return $block;
});

add_filter('sage/blocks/aanbod-overview/data', function ($block) {
    $query = array(
        'post_type' => 'aanbod',
        'orderby' => 'menu_order',
        'order' => 'ASC',
        'posts_per_page' => -1,
    );

    $block['aanbod'] = new WP_Query($query);
    return $block;
});

add_filter('sage/blocks/tab-group/data', function ($block) {
    $pid = get_post();
	if ( has_blocks( $pid ) ) {
        $blocks = parse_blocks( $pid->post_content );
		foreach ( $blocks as $blockEl ) {
			if ( $blockEl['blockName'] && $blockEl['blockName'] === 'acf/tab-group' ) {
                foreach($blockEl['innerBlocks'] as $innerBlock) {
                    if($innerBlock['blockName'] === 'acf/tab-content') {
                        $title = $innerBlock['attrs']['data']['title'] ?: 'Titel '. rand(0, 9999);
                        $block['nav'][sanitize_title($title)] = $title;
                    }
                }
			}
		}
    }
    
    return $block;
});

add_filter('acf/load_field/name=type_page', 'vizit_acf_load_post_types');
function vizit_acf_load_post_types( $field ) {
    $choices = get_post_types( array( 'show_in_nav_menus' => true ), 'objects' );
    foreach ( $choices as $post_type ) :
        $field['choices'][$post_type->name] = $post_type->labels->singular_name;
    endforeach;
    return $field;
}

/* Custom helper for event price */

function vizit_show_event_price($post_id) {
    if(empty($post_id)) {
        $post_id = get_the_ID();
    }
    
    $price = __('Gratis', 'vizit');
    $acf_price = get_field('price', );

    if( !empty( $acf_price ) ) {
        $price = '&eur; '. $acf_price;
    }

    echo $price;
}
add_action( 'show_event_price', 'vizit_show_event_price' );

/* Events page helper */

function vizit_get_paginated_links( $query ) {
    $currentPage = max( 1, get_query_var( 'paged', 1 ) );
    $pages = range( 1, max( 1, $query->max_num_pages ) );

    return array_map( function( $page ) use ( $currentPage ) {
        return ( object ) array(
            'isCurrent' => $page == $currentPage,
            'page' => $page,
            'url' => get_pagenum_link( $page )
        );
    }, $pages );
}

add_filter( 'redirect_canonical', 'vizit_disable_redirect_canonical' );
function vizit_disable_redirect_canonical( $redirect_url ) {
    if ( is_paged() && is_singular() ) $redirect_url = false; 
    return $redirect_url; 
}

function vizit_query_vars( $qvars ) {
    $qvars[] = 'categorie';
    $qvars[] = 'pagina';
    $qvars[] = 'tag';
    return $qvars;
}
add_filter( 'query_vars', 'vizit_query_vars' );

/**
 * Helper function for prettying up errors
 * @param string $message
 * @param string $subtitle
 * @param string $title
 */
$sage_error = function ($message, $subtitle = '', $title = '') {
    $title = $title ?: __('Sage &rsaquo; Error', 'sage');
    $footer = '<a href="https://roots.io/sage/docs/">roots.io/sage/docs/</a>';
    $message = "<h1>{$title}<br><small>{$subtitle}</small></h1><p>{$message}</p><p>{$footer}</p>";
    wp_die($message, $title);
};

/**
 * Ensure compatible version of PHP is used
 */
if (version_compare('7.1', phpversion(), '>=')) {
    $sage_error(__('You must be using PHP 7.1 or greater.', 'sage'), __('Invalid PHP version', 'sage'));
}

/**
 * Ensure compatible version of WordPress is used
 */
if (version_compare('4.7.0', get_bloginfo('version'), '>=')) {
    $sage_error(__('You must be using WordPress 4.7.0 or greater.', 'sage'), __('Invalid WordPress version', 'sage'));
}

/**
 * Ensure dependencies are loaded
 */
if (!class_exists('Roots\\Sage\\Container')) {
    if (!file_exists($composer = __DIR__.'/../vendor/autoload.php')) {
        $sage_error(
            __('You must run <code>composer install</code> from the Sage directory.', 'sage'),
            __('Autoloader not found.', 'sage')
        );
    }
    require_once $composer;
}

/**
 * Sage required files
 *
 * The mapped array determines the code library included in your theme.
 * Add or remove files to the array as needed. Supports child theme overrides.
 */
array_map(function ($file) use ($sage_error) {
    $file = "../app/{$file}.php";
    if (!locate_template($file, true, true)) {
        $sage_error(sprintf(__('Error locating <code>%s</code> for inclusion.', 'sage'), $file), 'File not found');
    }
}, ['helpers', 'setup', 'filters', 'admin']);

/**
 * Here's what's happening with these hooks:
 * 1. WordPress initially detects theme in themes/sage/resources
 * 2. Upon activation, we tell WordPress that the theme is actually in themes/sage/resources/views
 * 3. When we call get_template_directory() or get_template_directory_uri(), we point it back to themes/sage/resources
 *
 * We do this so that the Template Hierarchy will look in themes/sage/resources/views for core WordPress themes
 * But functions.php, style.css, and index.php are all still located in themes/sage/resources
 *
 * This is not compatible with the WordPress Customizer theme preview prior to theme activation
 *
 * get_template_directory()   -> /srv/www/example.com/current/web/app/themes/sage/resources
 * get_stylesheet_directory() -> /srv/www/example.com/current/web/app/themes/sage/resources
 * locate_template()
 * ├── STYLESHEETPATH         -> /srv/www/example.com/current/web/app/themes/sage/resources/views
 * └── TEMPLATEPATH           -> /srv/www/example.com/current/web/app/themes/sage/resources
 */
array_map(
    'add_filter',
    ['theme_file_path', 'theme_file_uri', 'parent_theme_file_path', 'parent_theme_file_uri'],
    array_fill(0, 4, 'dirname')
);
Container::getInstance()
    ->bindIf('config', function () {
        return new Config([
            'assets' => require dirname(__DIR__).'/config/assets.php',
            'theme' => require dirname(__DIR__).'/config/theme.php',
            'view' => require dirname(__DIR__).'/config/view.php',
        ]);
    }, true);

@extends('layouts.app')

@section('content')
  @while(have_posts()) @php the_post() @endphp
    @include('partials.page-header')

    <div class="grid-container" id="evenementen">
      <nav class="event-categories">
        <ul class="event-categories__list">
          <li><a href="/evenementen/" class="label @if(!get_query_var( 'categorie' )) active @endif">Alle evenementen</a></li>

          @foreach( $event_categories as $category )
            <li><a href="{{ add_query_arg( 'categorie', $category->slug ) }}" class="label @if(get_query_var( 'categorie' ) && get_query_var( 'categorie' ) === $category->slug) active @endif">{!! _e( $category->name ) !!}</a></li>
          @endforeach
        </ul>
      </nav>
    </div>
    @if($events->have_posts())
      @while( $events->have_posts() ) @php $events->the_post() @endphp
        @include('partials.content-'.get_post_type())
      @endwhile
    @else
          <p class="notice">{!! __('Ojee, er zijn nog geen evenementen ingepland de komende weken. Wil je er graag eentje samen organiseren? Neem dan snel <a href="/contact/">contact met ons op</a>!', 'vizit') !!}</p>
    @endif

    <div class="grid-container">
      <div class="pagination">
        <h6>pagina: </h6>
        <ul class="pagination__list">
          @foreach( vizit_get_paginated_links( $events ) as $link )
          <li>
              @if ( $link->isCurrent )
                <span class="btn-pagination btn-pagination--active">{!! _e( $link->page ) !!}</span>
              @else
                <a href="{{ add_query_arg( 'pagina', $link->page ) }}" class="btn-pagination btn-pagination--inactive">{!! _e( $link->page ) !!}</a>
              @endif
          </li>
          @endforeach
        </ul>
      </div>
    </div>

    @php wp_reset_postdata() @endphp

    @include('partials.content-page')
  @endwhile
@endsection

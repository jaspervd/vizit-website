{{--
  Title: Tab
  Description: Tab om overzichtelijker content te tonen in een tab groep
  Category: formatting
  Icon: table-col-after
  Keywords: tabs content
  Mode: preview
  PostTypes: page post aanbod ruimte
  SupportsAlign: false
  SupportsMultiple: true
  SupportsInnerBlocks: true
--}}

<article data-{{ $block['id'] }} class="{{ $block['classes'] }}" data-title="{{ get_field('title') }}" id="{{ sanitize_title(get_field('title')) }}">
  <InnerBlocks />
</article>

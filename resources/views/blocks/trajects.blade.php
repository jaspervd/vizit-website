{{--
  Title: Traject
  Description: Gelijkaardig aan de evenementen om trajecten uit te lichten
  Category: formattting
  Icon: portfolio
  Keywords: trajecten evenementen
  Mode: edit
  PostTypes: page post aanbod ruimte
  SupportsAlign: false
  SupportsMultiple: true
  SupportsInnerBlocks: true
--}}

<article data-{{ $block['id'] }} class="{{ $block['classes'] }} traject">
  <div class="traject__inner">
    <span class="traject__timing">{{ get_field('timing') }}</span>
    <h3 class="post__inner__title">{{ get_field('title') }}</h3>
    <div class="post__inner__content" itemprop="description">
      {!! get_field('content') !!}
    </div>
    <footer class="traject__inner__details">
      <div class="traject__inner__details__left">
        @php 
          $link = get_field('link');
        @endphp
        <a class="btn btn-primary" href="{{ esc_url( $link['url'] ) }}" target="@php echo $link['target'] ?: '_self' @endphp">{!! esc_html( $link['title'] ) !!}</a>
      </div>
      <div class="traject__inner__details__right">
        {!! get_field('extra_details') !!}
      </div>
    </footer>
  </div>
</article>

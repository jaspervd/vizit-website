{{--
  Title: Interactieve media
  Description: Afbeeldingen met achterliggende
  Category: formattting
  Icon: screenoptions
  Keywords: trajecten evenementen
  Mode: edit
  PostTypes: page post aanbod ruimte
  SupportsAlign: false
  SupportsMultiple: true
  SupportsInnerBlocks: true
--}}

<section data-{{ $block['id'] }} class="{{ $block['classes'] }}">
  @while(have_rows('media')) @php(the_row())
    <article class="interactive-media__item">
      <figure class="interactive-media__item__image">
        {!! wp_get_attachment_image( get_sub_field('image')['ID'], 'full' ) !!}
      </figure>
      <div class="interactive-media__item__content">
        {!! get_sub_field('content') !!}
      </div>
    </article>
  @endwhile
</section>

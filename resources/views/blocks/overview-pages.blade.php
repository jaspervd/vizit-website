{{--
  Title: Toon overzicht
  Description: Toon een overzicht van geselecteerd type pagina
  Category: formatting
  Icon: align-left
  Keywords: overview pages overzicht paginas
  Mode: edit
  PostTypes: page
  SupportsAlign: false
  SupportsMultiple: false
--}}

<section data-{{ $block['id'] }} class="{{ $block['classes'] }}">
  <div class="grid-container">
  @php $i = 1; @endphp
    @while($block['page']->have_posts()) @php $block['page']->the_post() @endphp
      @include('partials.content-overview')
      @php $i++; @endphp
    @endwhile
  </div>
</section>

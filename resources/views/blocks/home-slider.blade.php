{{--
  Title: Home slider
  Description: Toon de grote slider en stel slides in
  Category: formatting
  Icon: slides
  Keywords: homepage slider
  Mode: edit
  PostTypes: page
  SupportsAlign: false
  SupportsMultiple: false
--}}

<section data-{{ $block['id'] }} class="{{ $block['classes'] }}">
  <div class="swiper-container">
    <div class="swiper-wrapper">
    @while(have_rows('slides')) @php the_row() @endphp
      <article class="swiper-slide home-slider__slide">
        <div class="wp-block-media-text home-media-text alignwide is-stacked-on-mobile">
          <div class="wp-block-media-text__media home-slider-blob">
            <div class="wp-block-media-text__media__wrapper">{!! wp_get_attachment_image( get_sub_field('image')['ID'], 'full' ) !!}</div>
          </div>
          <div class="wp-block-media-text__content">
            <h2 class="home-slider__title">{{ get_sub_field('title') }}</h2>
            {!! get_sub_field('text') !!}
            @php $button = get_sub_field('button') @endphp
            <a href="{{ $button['url'] }}" class="btn">{{ $button['title'] }}</a>
          </div>
        </div>
      </article>
    @endwhile
    </div>
    <div class="swiper-button-prev btn-arrow btn-arrow--alt btn-arrow--left"></div>
    <div class="swiper-button-next btn-arrow btn-arrow--alt btn-arrow--right"></div>
  </div>
</section>

{{--
  Title: Iconen met tekst
  Description: Toon iconen vergezeld met tekst (vb voor de waarden)
  Category: formatting
  Icon: forms
  Keywords: icons iconen tekst text values waarden
  Mode: edit
  PostTypes: page post aanbod ruimte
  SupportsAlign: false
  SupportsMultiple: false
--}}

@if( have_rows('icons_text') )
  <section data-{{ $block['id'] }} class="{{ $block['classes'] }}">
    <div class="grid-container">
      <div class="grid-row">
        @while( have_rows('icons_text') ) @php the_row() @endphp
          <article class="icons-text__item">
            <div class="icons-text__item__icon">
              {!! wp_get_attachment_image( get_sub_field('icon')['ID'], 'full' ) !!}
            </div>
            <h3 class="icons-text__item__title">{{ get_sub_field('title') }}</h3>
            <div class="icons-text__item__content">
              {!! get_sub_field('content') !!}
            </div>
          </article>
        @endwhile
      </div>
    </div>
  </section>
@endif

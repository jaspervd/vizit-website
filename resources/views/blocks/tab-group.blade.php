{{--
  Title: Tab groep
  Description: Gebruik dit om aparte tabs in te groeperen
  Category: formatting
  Icon: shortcode
  Keywords: tabs groep group content
  Mode: preview
  PostTypes: page post aanbod ruimte
  SupportsAlign: false
  SupportsMultiple: true
  SupportsInnerBlocks: true
--}}

<section data-{{ $block['id'] }} class="{{ $block['classes'] }} tab-group--{{ get_field('layout') }}">
  <div class="grid-container tab-group__container">
    <nav class="tab-group__nav">
      <ul class="tab-group__nav__list">
        @foreach($block['nav'] as $slug => $navItem)
          <li class="tab-group__nav__list__item-wrapper">
            <a class="menu-item" href="#{{ $slug }}">{{ $navItem }}</a>
          </li>
        @endforeach
      </ul>
    </nav>
    <div class="tab-group__content-wrapper">
      <InnerBlocks />
    </div>
  </div>
</section>

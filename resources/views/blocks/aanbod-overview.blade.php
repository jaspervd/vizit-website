{{--
  Title: Aanbod overzicht
  Description: overzicht van het aanbod met fotos vanuit de galerij
  Category: formatting
  Icon: editor-alignleft
  Keywords: aanbod overzicht galerij gallery
  Mode: edit
  PostTypes: page
  SupportsAlign: false
  SupportsMultiple: false
--}}

<section data-{{ $block['id'] }} class="{{ $block['classes'] }}">
  <div class="aanbod-overview__gallery-container">
      @php $i = 1 @endphp
      @while( $block['aanbod']->have_posts() ) @php $block['aanbod']->the_post() @endphp
      <div class="aanbod-overview__gallery aanbod-gallery aanbod-gallery--{{ $i }}">
        @if(get_field('gallery', get_the_ID()))
          @foreach(get_field('gallery', get_the_ID()) as $image)
            {!! wp_get_attachment_image( $image['ID'], 'full', false, array('class' => 'aanbod-gallery__item') ) !!}
          @endforeach
        @endif
      </div>
      @php $i++ @endphp
      @endwhile
  </div>
  <nav class="aanbod-overview__menu-container">
    <div class="grid-container">
      <ul class="aanbod-overview__menu__list">
        @php $i = 1 @endphp
        @while( $block['aanbod']->have_posts() ) @php $block['aanbod']->the_post() @endphp
        <li class="aanbod-overview__menu__list__item">
          <a class="menu-item @if($i === 1) active @endif" href="{{ get_the_permalink() }}" data-gallery="{{ $i }}">
            <span class="aanbod-overview__menu__list__item__name">{{ get_the_title() }}</span>
            <span class="aanbod-overview__menu__list__item__subtitle">{{ get_field('subtitle', get_the_ID()) }}</span>
          </a>
        </li>
        @php $i++ @endphp
        @endwhile
      </ul>
    </div>
  </nav>
</section>

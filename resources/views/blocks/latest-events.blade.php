{{--
  Title: Opkomende evenementen
  Description: Toon de opkomende evenementen
  Category: formatting
  Icon: calendar-alt
  Keywords: events evenementen
  Mode: edit
  PostTypes: page post aanbod ruimte
  SupportsAlign: false
  SupportsMultiple: false
--}}

<section data-{{ $block['id'] }} class="{{ $block['classes'] }}">
  <div class="grid-container">
  @if($block['events'])
    @php global $post @endphp
    @foreach($block['events'] as $post)
      @php setup_postdata($post) @endphp
      @include('partials.content-'. get_post_type())
    @endforeach
    @php wp_reset_postdata() @endphp
  @else
    <p class="notice">{!! __('Ojee, er zijn nog geen evenementen ingepland de komende weken. Wil je er graag eentje samen organiseren? Neem dan snel <a href="/contact/">contact met ons op</a>!', 'vizit') !!}</p>
  @endif
  </div>
</section>

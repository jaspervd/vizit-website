{{--
  Title: Nieuwsheadlines
  Description: Toon de nieuwsheadlines
  Category: formatting
  Icon: admin-comments
  Keywords: nieuws artikels slider marquee headlines
  Mode: edit
  PostTypes: page
  SupportsAlign: false
  SupportsMultiple: false
--}}

@if($block['news']->have_posts())
<section data-{{ $block['id'] }} class="{{ $block['classes'] }}">
  <header class="news-headlines__title">
    <h2>{{ __('Nieuws', 'vizit') }}</h2>
  </header>
  <ul class="news-headlines__inner">
    @while($block['news']->have_posts()) @php $block['news']->the_post() @endphp
      <li><a href="{{ get_permalink(get_option( 'page_for_posts' )) }}#{{ get_post(get_the_ID())->post_name }}" class="news-headline">{{ the_title() }}</a></li>
    @endwhile
  </ul>
</section>

<style type="text/css">
  [data-{{$block['id']}}] .news-headlines__inner {
    -webkit-animation-duration: {{ get_field('count') * 4 }}s;
    -moz-animation-duration: {{ get_field('count') * 4 }}s;
    animation-duration: {{ get_field('count') * 4 }}s;
  }

  @media (min-width: 768px) {
    [data-{{$block['id']}}] .news-headlines__inner {
      -webkit-animation-duration: {{ get_field('count') * 7 }}s;
      -moz-animation-duration: {{ get_field('count') * 7 }}s;
      animation-duration: {{ get_field('count') * 7 }}s;
    }
  }

  @media (min-width: 992px) {
    [data-{{$block['id']}}] .news-headlines__inner {
      -webkit-animation-duration: {{ get_field('count') * 10 }}s;
      -moz-animation-duration: {{ get_field('count') * 10 }}s;
      animation-duration: {{ get_field('count') * 10 }}s;
    }
  }
</style>
@endif

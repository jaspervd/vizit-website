{{--
  Title: Partners
  Description: Toon partners naast elkaar
  Category: formatting
  Icon: share-alt
  Keywords: partners logos
  Mode: edit
  PostTypes: page post aanbod ruimte
  SupportsAlign: false
  SupportsMultiple: false
--}}

@if( have_rows('partners') )
<div class="grid-container">
  <ul data-{{ $block['id'] }} class="{{ $block['classes'] }}">
    @while( have_rows('partners') ) @php the_row() @endphp
      <li class="partners__item"><a href="{{ get_sub_field('url') }}" target="_blank" rel="noopener" title="{{ get_sub_field('partner') }}">{!! wp_get_attachment_image( get_sub_field('image')['ID'], 'full' ) !!}</a></li>
    @endwhile
  </ul>
</div>
@endif

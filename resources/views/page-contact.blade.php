@extends('layouts.app')

@section('content')
  @while(have_posts()) @php the_post() @endphp
    @include('partials.page-header')
    @include('partials.content-page')

  <div class="grid-container">
    <div class="grid-row">
      <section class="contact__form">
       <div class="contact__form__inner">
          {!! $contact_form !!}
        </div>
      </section>
      <section class="contact__opening-hours">
        {!! $opening_hours !!}
      </section>
    </div>
  </div>
  <div class="map"></div>
  @endwhile
@endsection

<div class="single-event__content">
  <div class="single-event__content__inner">
    @php echo get_the_content() @endphp
      @if(get_field('facebook_link', get_the_ID()))
        <p><strong><a href="{{ get_field('facebook_link', get_the_ID()) }}" target="_blank">{{ __('Bekijk op Facebook', 'vizit') }}</a></strong></p>
      @endif
    <footer class="event__inner__details">
      <div class="event__inner__details__time">
        {{ __('hoe laat?', 'vizit') }}<br />
        <strong>vanaf {{ eo_get_the_start('H\ui') }}</strong>
      </div>
      <div class="event__inner__details__where">
        {{ __('waar?', 'vizit') }}<br />
        @php $venues = get_post_meta(get_the_ID(), '_venues', true); @endphp
        <strong>{!! implode('</strong>, <strong>', $venues) !!}</strong>
      </div>
      @if(get_field('ticket_link', get_the_ID()))
      <div class="event__inner__details__tickets">
        {{ __('tickets?', 'vizit') }}<br />
        <strong><a href="{{ get_field('ticket_link', get_the_ID()) }}" target="_blank">{{ __('Tickets', 'vizit') }}</a></strong>
      </div>
      @endif
      <span class="event__price">@php do_action('show_event_price') @endphp</span>
    </div>
  </div>
</div>
@if(!empty($next_events))
<h2>{{ __('Meer evenementen', 'vizit') }}</h2>
<div class="grid-container">
  @php global $post @endphp
  @foreach($next_events as $post)
    @php setup_postdata($post) @endphp
    @include('partials.content-'.get_post_type())
  @endforeach
  @php wp_reset_postdata() @endphp
</div>
@endif

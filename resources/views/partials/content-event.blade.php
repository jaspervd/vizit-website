<article @php post_class('post') @endphp itemscope itemtype="http://schema.org/Event">
  <span class="event__price">@php do_action('show_event_price') @endphp</span>
  <time class="post__date" datetime="{{ eo_get_the_start('c') }}" itemprop="startDate"><span class="day">{{ eo_get_the_start('d') }}</span><span class="month">{{ eo_get_the_start('M') }}</span></time>
  <div class="post__inner">
    <h3 class="post__inner__title"><a href="{{ the_permalink() }}" itemprop="url"><span itemprop="summary">{!! get_the_title() !!}</span></a></h3>
    <div class="post__inner__content" itemprop="description">
      @php get_the_excerpt() @endphp
    </div>
    <footer class="event__inner__details">
      <div class="event__inner__details__time">
        {{ __('hoe laat?', 'vizit') }}<br />
        <strong>vanaf {{ eo_get_the_start('H\ui') }}</strong>
      </div>
      <div class="event__inner__details__where">
        {{ __('waar?', 'vizit') }}<br />
        @php $venues = get_post_meta(get_the_ID(), '_venues', true); @endphp
        <strong>{!! implode('</strong>, <strong>', $venues) !!}</strong>
      </div>
      @if(get_field('ticket_link'))
      <div class="event__inner__details__tickets">
        {{ __('tickets?', 'vizit') }}<br />
        <strong><a href="{{ get_field('ticket_link') }}" target="_blank">{{ __('Tickets', 'vizit') }}</a></strong>
      </div>
      @endif
      <a href="{{ the_permalink() }}" class="btn">{{ __('Lees meer', 'vizit') }}</a>
    </footer>
  </div>
</article>

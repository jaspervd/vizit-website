<header class="header">
  <div class="grid-container">
    <div class="nav-wrapper">
      <a class="logo-wrapper" href="{{ home_url('/') }}">{!! App::logo() !!}</a>
      <nav class="nav-primary">
        @if (has_nav_menu('primary_navigation'))
          {!! wp_nav_menu(['theme_location' => 'primary_navigation', 'menu_class' => 'nav']) !!}
        @endif
      </nav>
    </div>
  </div>
</header>
<button class="toggle-menu" data-toggle="nav-primary">
  <span class="toggle-menu__first-dash"></span>
  <span class="toggle-menu__second-dash"></span>
  <span class="toggle-menu__third-dash"></span>
</button>

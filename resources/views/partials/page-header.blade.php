<div class="page-header">
  <h1>{!! App::title() !!}</h1>
  @if(get_field('subtitle'))
    <h2>{!! get_field('subtitle') !!}</h1>
  @endif
  @if(is_single() && get_post_type() === 'event')
    <h2><time class="page-header__date" datetime="{{ eo_get_the_start('c') }}" itemprop="startDate"><span class="day">{{ eo_get_the_start('d') }}</span> <span class="month">{{ eo_get_the_start('F') }}</span></time></h2>
  @endif
  @if(is_single() && get_post_type() === 'aanbod')
    <div class="page-header__location">
      <div class="marker"></div>
      <a target="_blank" href="https://www.google.be/maps/dir//{{ htmlentities( get_field('address_street') .', '. get_field('address_postal_code') .' '. get_field('address_locality') ) }}">{{ get_field('address_street') }}, {{ get_field('address_postal_code') }} {{ get_field('address_locality') }}</a>
    </div>
  @endif
  @if(is_single() && get_post_type() === 'ruimte')
    <div class="page-header__short-description">
      {!! get_field('short_description') !!}
    </div>
    <a href="#reserveren" class="btn btn-primary">{{ __('Reserveren', 'vizit') }}</a>
  @endif
</div>

<div class="grid-container">
  <div class="grid-row">
    @if($post->post_name !== 'huren')
    <div class="single-ruimte__gallery">
      <div class="single-ruimte__gallery__inner">
        <div class="swiper-container">
          <div class="swiper-wrapper">
            <div class="swiper-slide">
              <div class="blob-container ruimte-slider-blob">
                <div class="wp-block-media-text__media__wrapper">
                  {!! the_post_thumbnail('full') !!}
                </div>
              </div>
            </div>
            @if(get_field('galerij'))
              @foreach(get_field('galerij') as $image)
                <div class="swiper-slide">
                  <div class="blob-container no-bg-blob">
                    <div class="wp-block-media-text__media__wrapper">
                      {!! wp_get_attachment_image( $image['ID'], 'full' ) !!}
                    </div>
                  </div>
                </div>
              @endforeach
            @endif
          </div>
          <div class="single-ruimte__gallery__navigation">
            <div class="swiper-button-prev btn-arrow btn-arrow--left"></div>
            <div class="swiper-button-next btn-arrow btn-arrow--right"></div>
          </div>
        </div>
      </div>
    </div>
    @endif
    <div class="single-ruimte__content">
      @php the_content() @endphp
    </div>
  </div>
</div>

<article @php post_class() @endphp id="{{ $post->post_name }}">
  <time class="post__date" datetime="{{ get_post_time('c', true) }}" itemprop="datePublished"><span class="day">{{ get_the_date('d') }}</span><span class="month">{{ get_the_date('M') }}</span></time>
  <div class="post__inner">
    <h3 class="post__inner__title">{!! get_the_title() !!}</h3>
    <div class="post__inner__content">
      @php the_content() @endphp
    </div>
  </div>
</article>

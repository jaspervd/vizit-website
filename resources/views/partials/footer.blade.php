<footer class="footer">
  <div class="grid-container">
    <div class="footer__top">
     @php dynamic_sidebar('footer-top') @endphp
    </div>
    <div class="footer__menus">
     @php dynamic_sidebar('footer-menus') @endphp
    </div>
    <div class="footer__bottom">
     @php dynamic_sidebar('footer-bottom') @endphp
     <div class="footer__bottom__closer">
        knutselwerk door <a href="https://jaspers.work" target="_blank" rel="noopener">Jasper</a>
     </div>
    </div>
  </div>
</footer>

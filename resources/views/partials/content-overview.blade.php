@php
  $classes = 'wp-block-media-text alignwide is-stacked-on-mobile';

  if($i % 2 === 0) {
    $classes .= ' has-media-on-the-right';
  }
  
  $description = ( get_field('short_description', get_the_ID() )? get_field('short_description', get_the_ID()) : '<p>' . get_the_excerpt() . '</p>' );
@endphp
<article @php post_class($classes) @endphp id="{{ $post->post_name }}">
  <div class="wp-block-media-text__media">
    <div class="wp-block-media-text__media__wrapper">{!! the_post_thumbnail('full') !!}</div>
  </div>
  <div class="wp-block-media-text__content">
    <h2>{!! get_the_title() !!}</h2>
    {!! $description !!}
    <a href="{{ the_permalink() }}" class="btn">{{ __('Lees meer', 'vizit') }}</a>
  </div>
</article>

@extends('layouts.app')

@section('content')
  @include('partials.page-header')

  <div class="grid-container">
  @if (!have_posts())
    <div class="alert alert-warning">
      {{ __('Sorry, de pagina die je probeerde te vinden bestaat niet (meer).', 'vizit') }}
    </div>
  @endif
  </div>
@endsection

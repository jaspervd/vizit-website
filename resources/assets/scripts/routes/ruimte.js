/* global ruimte */
import { Swiper, Navigation, Autoplay } from 'swiper';

export default {
  init() {
    Swiper.use([Navigation, Autoplay]);

    let swiperGallery = new Swiper('.single-ruimte__gallery .swiper-container', {
      speed: 750,
      observer: true,
      observeParents: true,
      autoplay: {
        delay: 5000,
      },
      //loop: true,
      navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
      },
    });

    const title = ruimte.title.toLowerCase().toString().trim().replace(/\u00AD/g,'');
    $('.fld-ruimte option').each((index, el) => {
      const val = $(el).val().toLowerCase().toString().trim();

      if(val.indexOf( title ) !== -1 ) {
        $(el).prop('selected', true);
        return false;
      }
    });
  },
  finalize() {
  },
};

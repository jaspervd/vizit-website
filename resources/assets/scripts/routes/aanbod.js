/* global map_data */
/* global mapboxgl */

export default {
  init() {
    let map;

    const setupMap = () => {
      mapboxgl.accessToken = 'pk.eyJ1IjoiamFzcGVydmQiLCJhIjoiY2tpeDh1Z20zM2NlajMzcDNjY3dmZWtoZSJ9.9CZBb5TYUNM4dp2_nFE5PQ';
      map = new mapboxgl.Map({
        style: 'mapbox://styles/jaspervd/ckhgidl6x1bjn19nuvo79lcdf',
        center: [map_data.center.lng, map_data.center.lat],
        zoom: 15,
        container: document.querySelector('.map'),
      });

      map.on('load', function() {
        map.resize();
        map.scrollZoom.disable();

        var el = document.createElement('div');
        el.className = 'marker';
        
        var popup = new mapboxgl.Popup({ offset: 30 }).setHTML(`${map_data.address.title}<br />${map_data.address.street}<br />${map_data.address.place}`);

        new mapboxgl.Marker(el)
        .setLngLat([map_data.center.lng, map_data.center.lat])
        .setPopup(popup)
        .addTo(map);
      });
    };

    setupMap();
  },
}; 

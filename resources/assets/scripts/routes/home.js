import { Swiper, Navigation, Autoplay } from 'swiper';
import { preloadImages } from '../aanbod/utils';
import MenuController from '../aanbod/menuController';

export default {
  init() {
    Swiper.use([Navigation, Autoplay]);

    let swiperGallery = new Swiper('.home-slider .swiper-container', {
      speed: 750,
      observer: true,
      observeParents: true,
      autoplay: {
        delay: 5000,
      },
      //loop: true,
      navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
      },
    });

    // Preload  images and fonts
    Promise.all([preloadImages('.aanbod-gallery__item')]).then(() => {
      // Initialize the MenuController
      new MenuController(document.querySelector('.aanbod-overview__menu__list'));
    });
  },
  finalize() {
  },
};

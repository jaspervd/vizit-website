import { TweenMax, TimelineMax, Sine } from 'gsap';
import { disableBodyScroll, enableBodyScroll } from 'body-scroll-lock';

export default {
  init() {
    // Tabs
    const toggleActiveTab = (tabGroupRef, href) => {
      if(tabGroupRef.querySelector('.menu-item.active')) {
        tabGroupRef.querySelector('.menu-item.active').classList.remove('active');
      }
      if(tabGroupRef.querySelector('.tab-content.active')) {
        tabGroupRef.querySelector('.tab-content.active').classList.remove('active');
      }
      tabGroupRef.querySelector(`.menu-item[href="${ href }"]`).classList.add('active');
      tabGroupRef.querySelector(href).classList.add('active');

      window.dispatchEvent(new Event('resize'));
    };
  
    const tabGroupCollection = document.querySelectorAll('.tab-group');
    tabGroupCollection.forEach(tabGroup => {
      // nav logica
      let tabGroupNav = tabGroup.querySelectorAll('.menu-item');
      tabGroupNav.forEach(tabGroupNavItem => {
        tabGroupNavItem.addEventListener('click', (e) => {
          e.preventDefault();
          toggleActiveTab(tabGroup, tabGroupNavItem.getAttribute('href'));
        });
      });

      // content logica
      let tabGroupContent = tabGroup.querySelectorAll('.tab-content');

      // make correct item active
      if(window.location.hash && tabGroup.querySelector(window.location.hash)) {
        toggleActiveTab(tabGroup, window.location.hash);
      } else {
        toggleActiveTab(tabGroup, tabGroupNav[0].getAttribute('href'));
      }
    });

    // Form

    const formRuimte = $('.single-ruimte .form-tabs');
    if(formRuimte) {
      const activityTarget = $('.single-ruimte .form-tabs .fld-subject-activity-target');
      const activityTargetText = activityTarget.html();
      $('.single-ruimte .form-tabs .fld-subject-activity').on('change', function(e) {
        let newVal = $(e.currentTarget).val() || activityTargetText;
        activityTarget.html(newVal.trim().toLowerCase());
      });
  
      $('.single-ruimte .form-tabs .btn-form-next').on('click', function(e) {
        toggleActiveTab(formRuimte[0], `#${ $(e.currentTarget).data('target') }`);
      });
    }

    document.querySelectorAll('a[href^="#"]').forEach(anchor => {
      anchor.addEventListener('click', function (e) {
            e.preventDefault();
    
            document.querySelector(this.getAttribute('href')).scrollIntoView({
                behavior: 'smooth',
            });
        });
    });

    // Toggle menu
  
    let menu_active = false;
    $('.toggle-menu').on('click', (e) => {
      $(e.currentTarget).toggleClass('active');
      $('.nav-primary').toggleClass('active');
      $('.header').removeClass('hide');

      menu_active = !menu_active;

      if(menu_active) {
        disableBodyScroll($('.nav')[0]);
      } else {
        enableBodyScroll($('.nav')[0]);
      }
    });
  
    // Blobs

    $('.wp-block-media-text__media').addClass('blob-container');
    const $blobsToBe = $('.blob-container');
    const body = document.querySelector('body');
    const homeSliderWrapper = document.querySelector('.home-slider .swiper-wrapper');
    const ruimteSliderContainer = document.querySelector('.single-ruimte__gallery .swiper-container');
    let countBlobs = 1;

    $blobsToBe.each((index, el) => {
      el.setAttribute('id', `blob-${countBlobs}`);

      let blobBgWrapper = document.createElement('div');
      blobBgWrapper.classList.add('blob-bg-wrapper');

      let svgBg = document.createElementNS('http://www.w3.org/2000/svg', 'svg');
      svgBg.setAttributeNS(null, 'viewBox', '0 0 1 1');
      svgBg.setAttributeNS(null, 'id', `blob-${countBlobs}-bg-wrapper`);

      let blob_bg = document.createElementNS('http://www.w3.org/2000/svg', 'path');
      blob_bg.setAttributeNS(null, 'id', `blob-${countBlobs}-bg`);
      blob_bg.setAttributeNS(null, 'class', 'blob-bg');
      blob_bg.setAttributeNS(null, 'clipPathUnits', 'objectBoundingBox');

      svgBg.appendChild(blob_bg);
      blobBgWrapper.appendChild(svgBg);

      if(homeSliderWrapper && $(el).hasClass('home-slider-blob')) {
        homeSliderWrapper.appendChild(blobBgWrapper);
      } else if(ruimteSliderContainer && $(el).hasClass('ruimte-slider-blob')) {
        ruimteSliderContainer.appendChild(blobBgWrapper);
      } else {
        body.appendChild(blobBgWrapper);
      }

      let svgClip = document.createElementNS('http://www.w3.org/2000/svg', 'svg');
      svgClip.setAttributeNS(null, 'viewBox', '0 0 100 100');
      svgClip.setAttributeNS(null, 'id', `blob-${countBlobs}-fg-wrapper`);
      svgClip.setAttributeNS(null, 'class', 'blob blob-fg-wrapper');

      let blob_fg = document.createElementNS('http://www.w3.org/2000/svg', 'path');
      blob_fg.setAttributeNS(null, 'id', `blob-${countBlobs}-fg`);
      blob_fg.setAttributeNS(null, 'class', 'blob-fg');

      let blog_clip = document.createElementNS('http://www.w3.org/2000/svg', 'clipPath');
      blog_clip.setAttributeNS(null, 'id', `blob-${countBlobs}-clip`);
      blog_clip.setAttributeNS(null, 'clipPathUnits', 'objectBoundingBox');
      blog_clip.setAttributeNS(null, 'class', 'blob-clip');

      blog_clip.appendChild(blob_fg);
      svgClip.appendChild(blog_clip);
      el.appendChild(svgClip);

      if(!$(el).hasClass('no-bg-blob')) {
        var blob1 = createBlob({
          element: document.querySelector(`#blob-${countBlobs}-bg`),
          numPoints: 6,
          centerX: 0.5,
          centerY: 0.5,
          minRadius: 0.4,
          maxRadius: 0.45,
          minDuration: 6,
          maxDuration: 20,
        });
      }

      var blob2 = createBlob({
        element: document.querySelector(`#blob-${countBlobs}-fg`),
        numPoints: 8,
        centerX: 0.5,
        centerY: 0.5,
        minRadius: 0.35,
        maxRadius: 0.5,
        minDuration: 5,
        maxDuration: 18,
      });

      let $blob = $(el);
      $blob.css('clip-path', `url(#blob-${countBlobs}-clip)`);
      //console.dir($(`#blob-${countBlobs}`));
      $(blobBgWrapper).css({ top: $blob.offset().top, left: $blob.offset().left, width: $blob.width(), height: $blob.height() });
      $(window).on('resize', () => {
        $(blobBgWrapper).css({ top: $blob.offset().top, left: $blob.offset().left, width: $blob.width(), height: $blob.height() });
      });

      countBlobs++;
    });

    function createBlob(options) {
      var points = [];
      var path = options.element;
      var slice = (Math.PI * 2) / options.numPoints;
      var startAngle = random(Math.PI * 2);

      var tl = new TimelineMax({
        onUpdate: update,
      });

      for (var i = 0; i < options.numPoints; i++) {
        var angle = startAngle + i * slice;
        var duration = random(options.minDuration, options.maxDuration);

        var point = {
          x: options.centerX + Math.cos(angle) * options.minRadius,
          y: options.centerY + Math.sin(angle) * options.minRadius,
        };

        var tween = TweenMax.to(point, duration, {
          x: options.centerX + Math.cos(angle) * options.maxRadius,
          y: options.centerY + Math.sin(angle) * options.maxRadius,
          repeat: -1,
          yoyo: true,
          ease: Sine.easeInOut,
        });

        tl.add(tween, -random(duration));
        points.push(point);
      }

      options.tl = tl;
      options.points = points;

      function update() {
        path.setAttribute('d', cardinal(points, true, 1));
      }

      return options;
    }

    // Cardinal spline - a uniform Catmull-Rom spline with a tension option
    function cardinal(data, closed, tension) {
      if (data.length < 1) return 'M0 0';
      if (tension == null) tension = 1;

      var size = data.length - (closed ? 0 : 1);
      var path = 'M' + data[0].x + ' ' + data[0].y + ' C';

      for (var i = 0; i < size; i++) {

        var p0, p1, p2, p3;

        if (closed) {
          p0 = data[(i - 1 + size) % size];
          p1 = data[i];
          p2 = data[(i + 1) % size];
          p3 = data[(i + 2) % size];

        } else {
          p0 = i == 0 ? data[0] : data[i - 1];
          p1 = data[i];
          p2 = data[i + 1];
          p3 = i == size - 1 ? p2 : data[i + 2];
        }

        var x1 = p1.x + (p2.x - p0.x) / 6 * tension;
        var y1 = p1.y + (p2.y - p0.y) / 6 * tension;

        var x2 = p2.x - (p3.x - p1.x) / 6 * tension;
        var y2 = p2.y - (p3.y - p1.y) / 6 * tension;

        path += ' ' + x1 + ' ' + y1 + ' ' + x2 + ' ' + y2 + ' ' + p2.x + ' ' + p2.y;
      }

      return closed ? path + 'z' : path;
    }

    function random(min, max) {
      if (max == null) { max = min; min = 0; }
      if (min > max) { var tmp = min; min = max; max = tmp; }
      return min + (max - min) * Math.random();
    }

    const scale = (num, in_min, in_max, out_min, out_max) => {
      return (num - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
    }
  },
  finalize() {
    setTimeout(() => {
      window.dispatchEvent(new Event('resize'));
    }, 150);

    $(window).one('scroll', () => {
      window.dispatchEvent(new Event('resize'));
    });

    var doc = document.documentElement;
    var w = window;

    var prevScroll = w.scrollY || doc.scrollTop;
    var curScroll;
    var direction = 0;
    var prevDirection = 0;

    var $header = $('.header');

    var checkScroll = function() {
      curScroll = w.scrollY || doc.scrollTop;

      if (curScroll > prevScroll) {
        direction = 2;
      }
      else if (curScroll < prevScroll) {
        direction = 1;
      }

      if (direction !== prevDirection) {
        toggleHeader(direction, curScroll);
      }

      prevScroll = curScroll;
    };

    const toggleHeader = (direction, curScroll) => {
      if (direction === 2 && curScroll > $header.height()) {
        $header.addClass('hide');
        prevDirection = direction;
      }
      else if (direction === 1) {
        $header.removeClass('hide');
        prevDirection = direction;
      }
    };

    window.addEventListener('scroll', checkScroll);
  },
};

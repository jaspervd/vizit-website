// import external dependencies
import 'jquery';

// Import everything from autoload
import './autoload/**/*'

// import local dependencies
import Router from './util/Router';
import common from './routes/common';
import home from './routes/home';
import contact from './routes/contact';
import singleAanbod from './routes/aanbod';
import singleRuimte from './routes/ruimte';

/** Populate Router instance with DOM routes */
const routes = new Router({
  common,
  home,
  contact,
  singleAanbod,
  singleRuimte,
});

// Load Events
jQuery(document).ready(() => routes.loadEvents());

import { calcWinsize, scrollIt } from './utils';
import {gsap} from 'gsap';
import MenuItem from './menuItem';

// Calculate the viewport size
let winsize = calcWinsize();
window.addEventListener('resize', () => winsize = calcWinsize());

export default class MenuController {
    constructor(el) {
        this.DOM = {el: el};
        // Set of small images each selected menu item has on the background
        this.DOM.galleries = [...document.querySelectorAll('.aanbod-overview__gallery-container > .aanbod-gallery')][0];
        // array of all MenuItems
        console.log(this.DOM.galleries);
        this.menuItems = [];
        [...this.DOM.el.querySelectorAll('.aanbod-overview__menu__list__item .menu-item')][0].forEach((item, pos) => {
            console.log(item);
            this.menuItems.push(new MenuItem(item, this.DOM.galleries[pos]));
        });
        this.init();
    }
    init() {
        // Current menu item index (starting with the first one).
        this.current = 0;
        // Highlight the current menu item
        this.menuItems[this.current].highlight();
        // Init/Bind events
        this.initEvents();
    }
    initEvents() {
        console.log(this.menuItems.entries());
        this.menuItems.forEach((item, pos) => {
            item.DOM.el.addEventListener('mouseenter', () => {
                if ( pos === this.current || this.isAnimating ) return;
                
                const direction = this.current < pos ? 'up' : 'down';

                this.toggleMenuItems(item, direction);

                // Update current value
                this.current = pos;
            });

            item.DOM.el.addEventListener('mouseleave', () => {
                this.isAnimating = false;
            });
        });
    }
    // Click/Select a menu item
    // Animate all the bg images out and animate the new menu item's in
    toggleMenuItems(upcomingItem, direction = 'up') {
        const currentItem = this.menuItems[this.current];
        const dir = direction === 'up' ? 1 : -1;
        
        currentItem.toggleCurrent();
        upcomingItem.toggleCurrent();
        
        gsap
        .timeline({
            defaults: {
                duration: 1, 
                ease: 'expo.inOut',
            },
            onStart: () => this.isAnimating = true,
            onComplete: () => this.isAnimating = false,
        })
        .to(currentItem.DOM.galleryItems, {
            y: dir*-winsize.height*1.2,
            stagger: dir*0.05,
            rotation: gsap.utils.random(-30,30),
        }, 0)
        .addLabel('upcomingImages', 0.1)
        .to(upcomingItem.DOM.galleryItems, {
            startAt: {y: dir*winsize.height*1.2, rotation: gsap.utils.random(-30,30)},
            y: 0,
            opacity: 1,
            rotation: 0,
            stagger: dir*0.05,
        }, 'upcomingImages');
    }
}

import {gsap} from 'gsap';

export default class MenuItem {
    constructor(el, galleryEl) {
        this.DOM = {
            el: el,
            gallery: galleryEl,
        };
        console.log(galleryEl);
        this.DOM.galleryItems = [...this.DOM.gallery.querySelectorAll('.aanbod-gallery__item')];
                
        this.isCurrent = false;
    }
    highlight() {
        this.toggleCurrent();

        gsap.to(this.DOM.galleryItems, {
            duration: 1, 
            ease: 'expo',
            startAt: {scale: 0.01, rotation: gsap.utils.random(-20,20)},
            scale: 1,
            opacity: +this.isCurrent,
            rotation: 0,
            stagger: 0.05,
        });
    }
    toggleCurrent() {
        this.DOM.el.classList[this.isCurrent ? 'remove' : 'add']('active');
        this.isCurrent = !this.isCurrent;
    }
}

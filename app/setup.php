<?php

namespace App;

use Roots\Sage\Container;
use Roots\Sage\Assets\JsonManifest;
use Roots\Sage\Template\Blade;
use Roots\Sage\Template\BladeProvider;

/**
 * Theme assets
 */
add_action('wp_head', function() {
    echo '<link rel="preconnect" href="https://fonts.gstatic.com" />';
}, 100);

add_action('wp_enqueue_scripts', function () {
    if(is_page( 'contact' ) || get_post_type() === 'aanbod') {
      wp_enqueue_script('mapbox-gl.js', 'https://api.mapbox.com/mapbox-gl-js/v0.54.0/mapbox-gl.js');
      wp_enqueue_style('mapbox-gl.css', 'https://api.mapbox.com/mapbox-gl-js/v0.54.0/mapbox-gl.css');
    }

    wp_enqueue_style('vizit/main.css', asset_path('styles/main.css'), false, null);
    wp_enqueue_script('vizit/main.js', asset_path('scripts/main.js'), ['jquery'], null, true);
    wp_enqueue_style( 'vizit/fonts', 'https://fonts.googleapis.com/css2?family=IBM+Plex+Mono:wght@500&family=Source+Sans+Pro:wght@400;600&display=swap', false );
}, 100);

/**
 * Setup extra data
 */

add_action('wp_enqueue_scripts', function() {
    if (is_single() && get_post_type() === 'ruimte') {
        $data = array(
            'title' => get_the_title(),
        );

        wp_localize_script( 'vizit/main.js', 'ruimte', $data );
    }
}, 100);

/* ACF Block CSS */

add_action('enqueue_block_editor_assets', function () {
  wp_enqueue_style('vizit/block-editor-css', asset_path('styles/gutenberg.css'), false, null);
});

/**
 * Setup map
 */

add_action('wp_enqueue_scripts', function() {
    if (is_page('contact')) {
        $data = array(
            'address' => array(
                'title' => get_bloginfo('name'),
                'street' => get_field('address_street', 'options'),
                'place' => get_field('address_postal_code', 'options') . ' ' . get_field('address_locality', 'options'),
            ),
            'center' => array(
                'lat' => get_field('address_coordinates_latitude', 'options'),
                'lng' => get_field('address_coordinates_longitude', 'options'),
            ),
        );

        wp_localize_script( 'vizit/main.js', 'map_data', $data );
    }

    if (is_single() && get_post_type() === 'aanbod') {
        $data = array(
            'address' => array(
                'title' => get_the_title(),
                'street' => get_field('address_street'),
                'place' => get_field('address_postal_code') . ' ' . get_field('address_locality'),
            ),
            'center' => array(
                'lat' => get_field('address_coordinates_latitude'),
                'lng' => get_field('address_coordinates_longitude'),
            ),
        );

        wp_localize_script( 'vizit/main.js', 'map_data', $data );
    }
}, 100);

/**
 * Theme setup
 */
add_action('after_setup_theme', function () {
    /**
     * Enable features from Soil when plugin is activated
     * @link https://roots.io/plugins/soil/
     */
    add_theme_support('soil-clean-up');
    add_theme_support('soil-jquery-cdn');
    add_theme_support('soil-nav-walker');
    add_theme_support('soil-nice-search');
    add_theme_support('soil-relative-urls');

    /**
     * Enable plugins to manage the document title
     * @link https://developer.wordpress.org/reference/functions/add_theme_support/#title-tag
     */
    add_theme_support('title-tag');

    /**
     * Register navigation menus
     * @link https://developer.wordpress.org/reference/functions/register_nav_menus/
     */
    register_nav_menus([
        'primary_navigation' => __('Hoofdmenu', 'sage')
    ]);

    /**
     * Enable post thumbnails
     * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
     */
    add_theme_support('post-thumbnails');

    /**
     * Enable HTML5 markup support
     * @link https://developer.wordpress.org/reference/functions/add_theme_support/#html5
     */
    add_theme_support('html5', ['caption', 'comment-form', 'comment-list', 'gallery', 'search-form']);

    /**
     * Enable selective refresh for widgets in customizer
     * @link https://developer.wordpress.org/themes/advanced-topics/customizer-api/#theme-support-in-sidebars
     */
    add_theme_support('customize-selective-refresh-widgets');

    /**
     * Use main stylesheet for visual editor
     * @see resources/assets/styles/layouts/_tinymce.scss
     */
    add_editor_style(asset_path('styles/main.css'));
}, 20);

/**
 * Register sidebars
 */
add_action('widgets_init', function () {
    $config = [
        'before_widget' => '<section class="footer-el %1$s %2$s">',
        'after_widget'  => '</section>',
        'before_title'  => '<h4>',
        'after_title'   => '</h4>'
    ];
    register_sidebar([
        'name'          => __('Footer Boven', 'sage'),
        'id'            => 'footer-top'
    ] + $config);
    register_sidebar([
        'name'          => __('Footer Menu\'s', 'sage'),
        'id'            => 'footer-menus'
    ] + $config);
    register_sidebar([
        'name'          => __('Footer Onder', 'sage'),
        'id'            => 'footer-bottom'
    ] + $config);
});

/**
 * Updates the `$post` variable on each iteration of the loop.
 * Note: updated value is only available for subsequently loaded views, such as partials
 */
add_action('the_post', function ($post) {
    sage('blade')->share('post', $post);
});

/**
 * Setup Sage options
 */
add_action('after_setup_theme', function () {
    /**
     * Add JsonManifest to Sage container
     */
    sage()->singleton('sage.assets', function () {
        return new JsonManifest(config('assets.manifest'), config('assets.uri'));
    });

    /**
     * Add Blade to Sage container
     */
    sage()->singleton('sage.blade', function (Container $app) {
        $cachePath = config('view.compiled');
        if (!file_exists($cachePath)) {
            wp_mkdir_p($cachePath);
        }
        (new BladeProvider($app))->register();
        return new Blade($app['view']);
    });

    /**
     * Create @asset() Blade directive
     */
    sage('blade')->compiler()->directive('asset', function ($asset) {
        return "<?= " . __NAMESPACE__ . "\\asset_path({$asset}); ?>";
    });
});

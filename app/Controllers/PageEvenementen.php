<?php

namespace App\Controllers;

use Sober\Controller\Controller;
use WP_Query;

class PageEvenementen extends Controller
{
  public function __construct() {
    if(get_query_var( 'categorie' )) {
      add_action( 'wp_footer', function() {
        ?>
        <script>
        document.addEventListener('readystatechange', (event) => {
          if (document.readyState === 'complete') { 
            var events = document.querySelector('#evenementen');
            events.scrollIntoView();
          }
        });
        </script>
        <?php
      });
    }
  }

  public function events() {
    $paged = ( get_query_var( 'pagina' ) ) ? absint( get_query_var( 'pagina' ) ) : 1;
    $cat = ( get_query_var( 'categorie' ) ) ? get_query_var( 'categorie' ) : '';
    $tag = ( get_query_var( 'type' ) ) ? get_query_var( 'type' ) : '';

    $args = array(
      'post_type'         => 'event',
      'event_start_after' => 'today',
      'numberposts'       => 3,
      'post__not_in'      => array(get_the_ID()),
      'posts_per_page'    => 9,
      'paged'             => $paged,
      'suppress_filters'  => false,
    );

    if($cat) {
      $args['tax_query'][] = array(
              'taxonomy'  => 'event-category',
              'operator'  => 'IN',
              'field'     => 'slug',
              'terms'     => $cat,
      );
    }

    if($tag) {
        $args['tax_query'][] = array(
                'taxonomy'  => 'event-tag',
                'operator'  => 'IN',
                'field'     => 'slug',
                'terms'     => $tag,
        );
    }

    $args['tax_query'][] = array(
      'taxonomy'  => 'event-tag',
      'operator'  => 'NOT IN',
      'field'     => 'slug',
      'terms'     => array('verhuur', 'repetitie'),
    );

    return new WP_Query($args);
  }

  public function eventCategories() {
    $query = array(
      'taxonomy' => 'event-category',
      'hide_empty' => false,
    );

    return get_terms($query);
  }

  public function eventTags() {
    $query = array(
      'taxonomy' => 'event-tag',
      'hide_empty' => false,
    );

    return get_terms($query);
  }
}

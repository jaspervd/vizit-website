<?php

namespace App\Controllers;

use Sober\Controller\Controller;

class App extends Controller
{
    public function siteName()
    {
        return get_bloginfo('name');
    }

    public static function title()
    {
        if (is_home()) {
            if ($home = get_option('page_for_posts', true)) {
                return get_the_title($home);
            }
            return __('Laatste berichten', 'vizit');
        }
        if (is_archive()) {
            return get_the_archive_title();
        }
        if (is_search()) {
            return sprintf(__('Zoekresultaten voor %s', 'vizit'), get_search_query());
        }
        if (is_404()) {
            return __('Pagina niet gevonden', 'vizit');
        }
        return get_the_title();
    }

    public static function logo() {
       $logo = get_field('logo', 'option');

       return wp_get_attachment_image($logo['ID'], 'full', true, array('class' => 'logo'));
    }
}

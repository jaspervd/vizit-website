<?php

namespace App\Controllers;

use Sober\Controller\Controller;

class SingleEvent extends Controller
{
  public function nextEvents() {
    $args = array(
      'event_start_after' => 'today',
      'numberposts'       => 3,
      'post__not_in'      => array(get_the_ID()),
    );

    return eo_get_events($args);
  }
}

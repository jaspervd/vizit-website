<?php

namespace App\Controllers;

use Sober\Controller\Controller;

class PageContact extends Controller
{
  public function contactForm() {
    $contact_form = '<p class="notice">'. __('Het contactformulier ontbreekt. Gelieve dit te herstellen.', 'vizit') .'</p>';

    if(get_field('contactform')) {
      $contact_form = do_shortcode('[contact-form-7 id="'. get_field('contactform') .'" title="Contactformulier"]');
    }

    return $contact_form;
  }

  public function openingHours() {
    $opening_hours = '<p class="notice">'. __('De openingsuren ontbreken. Gelieve dit te herstellen.', 'vizit') .'</p>';

    if(get_field('opening_hours')) {
      $opening_hours = get_field('opening_hours');
    }

    return $opening_hours;
  }
}
